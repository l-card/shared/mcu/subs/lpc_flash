#include "lpc_flash.h"
#include "lpc_flash_descr.h"
#include "chip.h"
#include "string.h"

#ifndef LPC_FLASH_WR_RETRY_CNT
    #define LPC_FLASH_WR_RETRY_CNT 5
#endif


static unsigned f_iap_command[6];
static unsigned f_iap_result[5];

/*****************************************************************************************
 *  получение номера сектора по адресу во flash (возвращает 0xFF, если адрес не во флеше)
 *****************************************************************************************/
static t_lpc_flash_errs f_get_flash_sector(const t_flash_bank *flashbanks, uint32_t addr, unsigned *sect, unsigned *bank) {
    int fnd=0;
    unsigned fnd_sect=0, fnd_bank=0;

    for (unsigned cur_bank = 0; !fnd && (flashbanks[cur_bank].arrays_cnt!=0); cur_bank++) {
        if ((addr >= flashbanks[cur_bank].addr) && ((flashbanks[cur_bank+1].arrays_cnt==0)
                                                   || (addr < flashbanks[cur_bank+1].addr))) {
            unsigned addr_offs = addr - flashbanks[cur_bank].addr;
            fnd_bank = cur_bank;            
            for (unsigned i=0, group_start_sect = 0; (i < flashbanks[cur_bank].arrays_cnt) && !fnd; i++)  {
                unsigned group_sec_num = addr_offs/flashbanks[cur_bank].arrays[i].sector_size;
                if (group_sec_num < flashbanks[cur_bank].arrays[i].sectors_cnt) {
                    fnd_sect = group_start_sect + group_sec_num;
                    fnd = 1;
                } else {
                    group_start_sect += flashbanks[cur_bank].arrays[i].sectors_cnt;
                    addr_offs -= (flashbanks[cur_bank].arrays[i].sectors_cnt *
                                  flashbanks[cur_bank].arrays[i].sector_size);
                }
            }
        }
    }

    if (fnd) {
        *sect = fnd_sect;
        *bank = fnd_bank;
    }

    return fnd ? LPC_FLASH_ERR_SUCCESS : LPC_FLASH_ERR_INVALID_SECTOR;
}

#ifdef IAP_INIT_CMD
static t_lpc_flash_errs f_iap_init(void) {
    static int f_init_done = 0;
    t_lpc_flash_errs err = LPC_FLASH_ERR_SUCCESS;
    if (!f_init_done) {
        IAP_ENTRY_T iap_entry =(IAP_ENTRY_T) IAP_LOCATION;
        f_iap_command[0] = IAP_INIT_CMD;
        iap_entry (f_iap_command, f_iap_result);
        f_init_done = 1;
    }
    return err;
}
#else
    #define f_iap_init()
#endif


t_lpc_flash_errs lpc_flash_erase(uint32_t start_addr, uint32_t last_addr) {
    t_lpc_flash_errs err = LPC_FLASH_ERR_SUCCESS;
    unsigned first_sect, first_bank, last_sect, last_bank;
    IAP_ENTRY_T iap_entry =(IAP_ENTRY_T) IAP_LOCATION;

    err = f_get_flash_sector(chip_flash_banks, start_addr, &first_sect, &first_bank);
    if (!err)
        err = f_get_flash_sector(chip_flash_banks, last_addr, &last_sect, &last_bank);

    if (!err && (first_bank != last_bank))
        err = LPC_FLASH_ERR_INVALID_SECTOR;

    if (!err) {

        __disable_irq();

        f_iap_init();

        for (unsigned i=0, done=0; (i < LPC_FLASH_WR_RETRY_CNT) && !done; i++) {
            f_iap_command[0] = IAP_PREWRRITE_CMD;
            f_iap_command[1] = first_sect; //sector
            f_iap_command[2] = last_sect;
            f_iap_command[3] = first_bank;
            iap_entry (f_iap_command, f_iap_result);
            if (f_iap_result[0] == 0)
                done = 1;
        }

        if (f_iap_result[0] == 0) {
            for (unsigned i=0, done=0; (i < LPC_FLASH_WR_RETRY_CNT) && !done; i++) {
                f_iap_command[0] = IAP_ERSSECTOR_CMD;
                f_iap_command[1] = first_sect; //sector
                f_iap_command[2] = last_sect;
                f_iap_command[3] = LPC_SYSCLK/1000; //clk (kHz)
                f_iap_command[4] = first_bank;
                iap_entry (f_iap_command, f_iap_result);
                if (f_iap_result[0] == 0)
                    done = 1;
            }
        }
        __enable_irq();

        if (f_iap_result[0]!=0)
            err = LPC_FLASH_ERR_ERASE;
    }
    return err;
}


t_lpc_flash_errs lpc_flash_write(uint32_t  addr, const uint8_t *buf, unsigned size) {
    t_lpc_flash_errs err = LPC_FLASH_ERR_SUCCESS;
    unsigned sector, bank;
    IAP_ENTRY_T iap_entry =(IAP_ENTRY_T) IAP_LOCATION;

    err = f_get_flash_sector(chip_flash_banks,  addr, &sector, &bank);

    if (!err) {        
        __disable_irq();

        f_iap_init();

        for (unsigned i=0, done=0; (i < LPC_FLASH_WR_RETRY_CNT) && !done; i++) {
            f_iap_command[0] = IAP_PREWRRITE_CMD;
            f_iap_command[1] = sector; //sector
            f_iap_command[2] = sector;
            f_iap_command[3] = bank;
            iap_entry (f_iap_command, f_iap_result);
            if (f_iap_result[0] == 0) {
                done = 1;
            }
        }

        if (f_iap_result[0] == 0) {
            for (unsigned i=0, done=0; (i < LPC_FLASH_WR_RETRY_CNT) && !done; i++) {
                //copy RAM to ROM
                f_iap_command[0] = IAP_WRISECTOR_CMD;
                f_iap_command[1] =  addr; //dest
                f_iap_command[2] = (uint32_t)buf; //src
                f_iap_command[3] = size;   //size
                f_iap_command[4] = LPC_SYSCLK/1000; //clk freq (kHz)
                f_iap_command[5] = bank;
                iap_entry (f_iap_command, f_iap_result);
                if (f_iap_result[0] == 0) {
                    done = 1;
                }
            }
        }

        __enable_irq();

        if (f_iap_result[0]!=0) {
            err = LPC_FLASH_ERR_WRITE;
        } else if (memcmp((void*) addr, buf, size)) {
            err = LPC_FLASH_ERR_COMPARE;
        }
    }
    return err;
}
