# makefile для включения в проект.
#
# на входе принимает параметры:
#  TARGET_CPU (или может быть переопределен через LPC_FLASH_TARGET) - определяет,
#              какой порт будет использоваться. Должен быть задан один
#              из поддерживаемых портов
#  LPC_FLASH_DIR  - путь к данному файлу относительно директории сборки проекта
#             (по-умолчанию lib/lpc_flash)
#
# устанавливает следующие параметры:
#  LPC_FLASH_SRC      - исходные файлы на C
#  LPC_FLASH_INC_DIRS - директории с заголовками, которые должны быть добавлены
#                     в список директорий для поиска заголовков проекта

LPC_FLASH_DIR ?= ./lib/lpc_flash
LPC_FLASH_TARGET ?= $(TARGET_CPU)


LPC_FLASH_INC_DIRS := $(LPC_FLASH_DIR)

LPC_FLASH_SRC  = $(LPC_FLASH_DIR)/lpc_flash.c \
                $(LPC_FLASH_DIR)/lpc_flash_descr_$(LPC_FLASH_TARGET).c
