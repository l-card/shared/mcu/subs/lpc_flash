#ifndef LPC_FLASH_DESCR_H_
#define LPC_FLASH_DESCR_H_

typedef struct {
    unsigned sectors_cnt;
    unsigned sector_size;
} t_flash_sectors_array;

typedef struct {
    unsigned addr;
    unsigned arrays_cnt;
    const t_flash_sectors_array *arrays;
} t_flash_bank;

extern const t_flash_bank chip_flash_banks[];

#endif
