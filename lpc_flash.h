#ifndef LPC_FLASH_H
#define LPC_FLASH_H

#include <stdint.h>

typedef enum {
    LPC_FLASH_ERR_SUCCESS = 0,
    LPC_FLASH_ERR_INVALID_SECTOR = -1,
    LPC_FLASH_ERR_ERASE          = -2,
    LPC_FLASH_ERR_WRITE          = -3,
    LPC_FLASH_ERR_COMPARE        = -4,
} t_lpc_flash_errs;


t_lpc_flash_errs lpc_flash_erase(uint32_t start_addr, uint32_t last_addr);
t_lpc_flash_errs lpc_flash_write(uint32_t  addr, const uint8_t *buf, unsigned size);

#endif // LPC_FLASH_H

