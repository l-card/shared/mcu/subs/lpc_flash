#include "lpc_flash_descr.h"
#include <stddef.h>

static const t_flash_sectors_array f_sectors[] = {
    { 8,   8*1024 },
    { 7,  64*1024 }
};

const t_flash_bank chip_flash_banks[] = {
    {0x1A000000, sizeof(f_sectors)/sizeof(f_sectors[0]), f_sectors},
    {0x1B000000, sizeof(f_sectors)/sizeof(f_sectors[0]), f_sectors},
    {0, 0, NULL}
};
